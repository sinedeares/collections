﻿using System;
using System.Collections;
using System.Collections.Generic;
using Collections.Interfaces;

namespace Collections
{
	public class DynamicArray<T> : IDynamicArray<T>
	{
		public DynamicArray()
		{
			throw new NotImplementedException();
		}

		public DynamicArray(int capacity)
		{
			throw new NotImplementedException();
		}

		public DynamicArray(IEnumerable<T> items)
		{
			throw new NotImplementedException();
		}

		public IEnumerator<T> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public T this[int index] => throw new NotImplementedException();

		public int Length { get; }
		public int Capacity { get; }
		public void Add(T item)
		{
			throw new NotImplementedException();
		}

		public void AddRange(IEnumerable<T> items)
		{
			throw new NotImplementedException();
		}

		public void Insert(T item, int index)
		{
			throw new NotImplementedException();
		}

		public bool Remove(T item)
		{
			throw new NotImplementedException();
		}
	}
}